//
// myStack.cpp for stack in /home/charti_t/test/cpp_abstractvm/files
// 
// Made by Thomas Chartier
// Login   <charti_t@epitech.net>
// 
// Started on  Sat Feb 21 12:10:19 2015 Thomas Chartier
// Last update Sun Feb 22 16:38:15 2015 Thomas Chartier
//

#include <sstream>
#include "myStack.hpp"
#include "myVm.hpp"

myStack::myStack()
{
}

myStack::~myStack()
{
}

void	myStack::push(std::string const &str)
{
  if (str != "")
    this->myQ.push (str);
}


void	myStack::dump()
{
  while (!myQ.empty())
    {
      std::cout << myQ.front() << std::endl;
      myQ.pop();
    }
}

void	myStack::doTask()
{
  std::istringstream	iss;
  std::string		cmd;
  std::string		typeArg;
  std::string		value;
  myVm			vm;
  
  while(!myQ.empty())
    {
      iss.str(this->myQ.front());
      iss >> cmd >> typeArg >> value >> std::ws;
      if (cmd == "exit" || cmd == ";;")
	break ;
      if (typeArg == "")
	  vm.exeCmd(cmd);
      else
	vm.exeCmdArg(cmd, typeArg, value);
      this->myQ.pop();
      iss.clear();
      cmd = "";
      typeArg = "";
      value = "";
    }
}
