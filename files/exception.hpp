//
// exception.hpp for exception in /home/charti_t/test/cpp_abstractvm/files
// 
// Made by Thomas Chartier
// Login   <charti_t@epitech.net>
// 
// Started on  Tue Feb 17 16:29:41 2015 Thomas Chartier
// Last update Tue Feb 17 17:02:26 2015 Thomas Chartier
//

#ifndef EXCEPTION_HPP_
# define EXCEPTION_HPP_

#include <string>
#include <exception>

class myException : public std::exception
{
 public:
  myException(std::string const &msg) throw();
  virtual ~myException() throw();
  virtual const char* what() const throw();
 private:
  std::string _msg;
};

#endif /* !EXCEPTION_HPP_ */
