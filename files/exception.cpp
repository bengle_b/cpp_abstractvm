//
// exception.cpp for exception in /home/charti_t/test/cpp_abstractvm/files
// 
// Made by Thomas Chartier
// Login   <charti_t@epitech.net>
// 
// Started on  Tue Feb 17 16:29:48 2015 Thomas Chartier
// Last update Tue Feb 17 17:05:29 2015 Thomas Chartier
//

#include <exception>
#include "exception.hpp"

myException::myException(std::string const &msg) throw()
  :_msg(msg)
{
}

myException::~myException() throw()
{
}

const char* myException::what() const throw()
{
  return(this->_msg.c_str());
}
