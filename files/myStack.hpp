//
// myStack.hpp for mystack in /home/charti_t/test/cpp_abstractvm/files
// 
// Made by Thomas Chartier
// Login   <charti_t@epitech.net>
// 
// Started on  Sat Feb 21 12:10:09 2015 Thomas Chartier
// Last update Sat Feb 21 16:24:12 2015 Thomas Chartier
//

#ifndef MYSTACK_HPP_
# define MYSTACK_HPP_

#include <map>
#include <queue>
#include <iostream>

class	myStack
{
public:
  myStack();
  virtual ~myStack();

  void	push(std::string const &str);
  void	dump();
  void	doTask();
  void	add();
private:
  std::queue<std::string> myQ;
};
#endif /* !MYSTACK_HPP_ */
