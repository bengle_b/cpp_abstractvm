//
// IOperand.hpp for IOperand in /home/bengle_b/rendu/cpp_abstractvm/files
// 
// Made by Bengler Bastien
// Login   <bengle_b@epitech.net>
// 
// Started on  Mon Feb 16 15:23:28 2015 Bengler Bastien
// Last update Mon Feb 23 10:18:15 2015 Bengler Bastien
//

#ifndef IOPERAND_HPP_
# define IOPERAND_HPP_

#include <iostream>

enum	eOperandType
  {
    Int8,
    Int16,
    Int32,
    Float,
    Double
  };

enum	eOperation
  {
    ADD,
    SOUS,
    MUL,
    DIV,
    MOD
  };

class	IOperand
{
public:

  virtual std::string const & toString() const = 0;

  virtual int getPrecision() const = 0;
  virtual eOperandType getType() const = 0;
  
  virtual IOperand * operator+(const IOperand &rhs) const = 0;
  virtual IOperand * operator-(const IOperand &rhs) const = 0;
  virtual IOperand * operator*(const IOperand &rhs) const = 0;
  virtual IOperand * operator/(const IOperand &rhs) const = 0;
  virtual IOperand * operator%(const IOperand &rhs) const = 0;

  virtual ~IOperand() {}
};

#endif /* !IOPERAND_HPP_ */
