//
// myVm.hpp for vm in /home/charti_t/test/cpp_abstractvm/files
// 
// Made by Thomas Chartier
// Login   <charti_t@epitech.net>
// 
// Started on  Sat Feb 21 16:07:26 2015 Thomas Chartier
// Last update Mon Feb 23 10:59:57 2015 Thomas Chartier
//
#ifndef MYVM_HPP_
# define MYVM_HPP_

#include <map>
#include <queue>
#include <iostream>
#include "OperandNew.hpp"
#include "OperandTemplate.hpp"
#include "my_OperationT.hpp"

class	myVm
{
public:
  myVm();
  virtual ~myVm();

  void	push(std::string const &arg, std::string const &value);
  void	pop();
  void	dump();
  void	assert(std::string const &arg, std::string const &value);
  void	add();
  void	sub();
  void	mul();
  void	div();
  void	mod();
  void	print();

  IOperand *createInt8(std::string const &str);
  IOperand *createInt16(std::string const &str);
  IOperand *createInt32(std::string const &str);
  IOperand *createFloat(std::string const &str);
  IOperand *createDouble(std::string const &str);

  void  exeCmd(std::string const &cmd);
  void  exeCmdArg(std::string const &,std::string const &,std::string const &);

private:
  std::queue<IOperand *> vmstack;
  std::map<std::string,void (myVm::*)()> ptrFunc;
  std::map<std::string,IOperand*(myVm::*)(std::string const &)> createOp;
};

#endif /* !MYVM_HPP_ */
