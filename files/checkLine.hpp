//
// checkLine.hpp for checkLine in /home/charti_t/test/cpp_abstractvm
// 
// Made by Thomas Chartier
// Login   <charti_t@epitech.net>
// 
// Started on  Tue Feb 17 17:10:01 2015 Thomas Chartier
// Last update Sat Feb 21 12:03:21 2015 Thomas Chartier
//

#ifndef CHECKLINE_H_
# define CHECKLINE_H_

#include <iostream>
#include <map>

class checkLine
{
public:
  checkLine(std::string const &line);
  virtual ~checkLine();

  bool		validFunc();
  bool		checkArg();
  bool		checkValue(std::string &arg);
  bool		checkNbr(std::string &arg);
  bool		isNbr(std::string &nbr);
  bool		checkLimit(std::string const &nbr);
  std::string	getCmd();
  std::string	getTypeArg();
  std::string	getValue();
  std::string	getInstr();
private:
  std::map<std::string,int>	_myFunc;
  std::string			_line;
  std::string			_instr;
  std::string			_cmd;
  std::string			_typeArg;
  std::string			_value;
};

#endif /* !CHECKLINE_H_ */
