//
// OperandTemplate.cpp for OperandTemplete in /home/bengle_b/rendu/cpp_abstractvm/files
// 
// Made by Bengler Bastien
// Login   <bengle_b@epitech.net>
// 
// Started on  Tue Feb 17 11:48:24 2015 Bengler Bastien
// Last update Mon Feb 23 10:19:35 2015 Bengler Bastien
//

#include <iostream>
#include <sstream>
#include <string>
#include "OperandTemplate.hpp"
#include "OperandNew.hpp"

template<typename T>
OperandTemplate<T>::OperandTemplate(const std::string & value, eOperandType precision)
  : _string_value(value), _precision(precision)
{
}

template<typename T>
OperandTemplate<T>::~OperandTemplate()
{
}

template<typename T>
std::string	const &OperandTemplate<T>::toString() const
{
  return _string_value;
}

template<typename T>
int	OperandTemplate<T>::getPrecision () const
{
  return _precision;
}

template<typename T>
eOperandType	OperandTemplate<T>::getType() const
{
  return _precision;
}

template<typename T>
IOperand * OperandTemplate<T>::operator+(const IOperand &rhs) const
{
  OperandNew toCreate;

  if (this->getPrecision() > rhs.getPrecision())
    return toCreate.ProcessOperation(this->getType(), this, &rhs, ADD);
  else
    return toCreate.ProcessOperation(rhs.getType(), &rhs, this, ADD);
}

template<typename T>
IOperand * OperandTemplate<T>::operator-(const IOperand &rhs) const
{
  OperandNew toCreate;

  if (this->getPrecision() > rhs.getPrecision())
    return toCreate.ProcessOperation(this->getType(), this, &rhs, SOUS);
  else
    return toCreate.ProcessOperation(rhs.getType(), this, &rhs, SOUS);
}

template<typename T>
IOperand * OperandTemplate<T>::operator*(const IOperand &rhs) const
{
  OperandNew toCreate;

  if (this->getPrecision() > rhs.getPrecision())
    return toCreate.ProcessOperation(this->getType(), this, &rhs, MUL);
  else
    return toCreate.ProcessOperation(rhs.getType(), this, &rhs, MUL);
}

template<typename T>
IOperand * OperandTemplate<T>::operator/(const IOperand &rhs) const
{
  OperandNew toCreate;

  if (this->getPrecision() > rhs.getPrecision())
    return toCreate.ProcessOperation(this->getType(), this, &rhs, DIV);
  else
    return toCreate.ProcessOperation(rhs.getType(), this, &rhs, DIV);
}

template<typename T>
IOperand * OperandTemplate<T>::operator%(const IOperand &rhs) const
{
  OperandNew toCreate;

  if (this->getPrecision() > rhs.getPrecision())
    return toCreate.ProcessOperation(this->getType(), this, &rhs, MOD);
  else
    return toCreate.ProcessOperation(rhs.getType(), this, &rhs, MOD);
}

template OperandTemplate<char>::OperandTemplate(const std::string & value, eOperandType precision);
template OperandTemplate<char>::~OperandTemplate();
template int OperandTemplate<char>::getPrecision() const;
template eOperandType OperandTemplate<char>::getType() const;
template IOperand * OperandTemplate<char>::operator+(const IOperand &rhs) const;
template IOperand * OperandTemplate<char>::operator-(const IOperand &rhs) const;
template IOperand * OperandTemplate<char>::operator*(const IOperand &rhs) const;
template IOperand * OperandTemplate<char>::operator/(const IOperand &rhs) const;

template OperandTemplate<short int>::OperandTemplate(const std::string & value, eOperandType precision);
template OperandTemplate<short int>::~OperandTemplate();
template int OperandTemplate<short int>::getPrecision() const;
template eOperandType OperandTemplate<short int>::getType() const;
template IOperand * OperandTemplate<short int>::operator+(const IOperand &rhs) const;
template IOperand * OperandTemplate<short int>::operator-(const IOperand &rhs) const;
template IOperand * OperandTemplate<short int>::operator*(const IOperand &rhs) const;
template IOperand * OperandTemplate<short int>::operator/(const IOperand &rhs) const;

template OperandTemplate<int>::OperandTemplate(const std::string & value, eOperandType precision);
template OperandTemplate<int>::~OperandTemplate();
template int OperandTemplate<int>::getPrecision() const;
template eOperandType OperandTemplate<int>::getType() const;
template IOperand * OperandTemplate<int>::operator+(const IOperand &rhs) const;
template IOperand * OperandTemplate<int>::operator-(const IOperand &rhs) const;
template IOperand * OperandTemplate<int>::operator*(const IOperand &rhs) const;
template IOperand * OperandTemplate<int>::operator/(const IOperand &rhs) const;

template OperandTemplate<float>::OperandTemplate(const std::string & value, eOperandType precision);
template OperandTemplate<float>::~OperandTemplate();
template int OperandTemplate<float>::getPrecision() const;
template eOperandType OperandTemplate<float>::getType() const;
template IOperand * OperandTemplate<float>::operator+(const IOperand &rhs) const;
template IOperand * OperandTemplate<float>::operator-(const IOperand &rhs) const;
template IOperand * OperandTemplate<float>::operator*(const IOperand &rhs) const;
template IOperand * OperandTemplate<float>::operator/(const IOperand &rhs) const;

template OperandTemplate<double>::OperandTemplate(const std::string & value, eOperandType precision);
template OperandTemplate<double>::~OperandTemplate();
template int OperandTemplate<double>::getPrecision() const;
template eOperandType OperandTemplate<double>::getType() const;
template IOperand * OperandTemplate<double>::operator+(const IOperand &rhs) const;
template IOperand * OperandTemplate<double>::operator-(const IOperand &rhs) const;
template IOperand * OperandTemplate<double>::operator*(const IOperand &rhs) const;
template IOperand * OperandTemplate<double>::operator/(const IOperand &rhs) const;
