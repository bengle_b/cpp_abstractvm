//
// checkLine.cpp for checkLine in /home/charti_t/test/cpp_abstractvm
// 
// Made by Thomas Chartier
// Login   <charti_t@epitech.net>
// 
// Started on  Tue Feb 17 17:10:10 2015 Thomas Chartier
// Last update Mon Feb 23 16:24:41 2015 Thomas Chartier
//

#include <iostream>
#include <sstream>
#include <map>
#include <limits>
#include "checkLine.hpp"
#include "exception.hpp"

checkLine::checkLine(std::string const &line)
  :_line(line)
{
  _myFunc["push"] = 1;
  _myFunc["pop"] = 0;
  _myFunc["dump"] = 0;
  _myFunc["assert"] = 1;
  _myFunc["add"] = 0;
  _myFunc["sub"] = 0;
  _myFunc["mul"] = 0;
  _myFunc["div"] = 0;
  _myFunc["mod"] = 0;
  _myFunc["print"] = 0;
  _myFunc["exit"] = 0;
  _myFunc["int8"] = 2;
  _myFunc["int16"] = 2;
  _myFunc["int32"] = 2;
  _myFunc["float"] = 2;
  _myFunc["double"] = 2;
  validFunc();
}

checkLine::~checkLine()
{

}


bool		checkLine::isNbr(std::string &nbr)
{
  int		i = 0;
  int		j = 0;
  int		key = 0;
  std::string	check  = "0123456789.";
  
  if (nbr[i] == '-')
    i++;
  while(nbr[i])
    {
      while(check[j])
        {
          if(nbr[i] == check[j])
            key = 1;
	  j++;
        }
      if (key == 0)
        return false;
      j = 0;
      key = 0;
      i++;
    }
  return true;
}

bool		checkLine::checkNbr(std::string &arg)
{
  int		min = arg.find("(", 0);
  int		max = arg.find(")", min + 1);
  int		dot = 0;
  std::string	nbr;
  int		tmp = min;

  if (max == -1 || min == -1)
    return false;
  while(min < max - 1)
    {
      if (arg.at(min) == '.')
	dot += 1;
      if(arg.at(min) == '-' && min != tmp + 1)
	return false;
      nbr += arg.at(++min);
    }
    if (dot > 1 
	|| arg.at(tmp + 1) == '.'
	|| arg.at(max - 1) == '.')
      return false;
    if (dot == 1 && (this->_typeArg != "float" && this->_typeArg != "double"))
      return false;
    if (isNbr(nbr) == true)
      {
	this->_value = nbr;
	this->_instr = _cmd + " " + _typeArg + " " + _value;
	return true;
      }
    return false;
}

bool		checkLine::checkValue(std::string &arg)
{
  char		typeArg[20];
  std::string	valueArg;

  if (arg == "" || arg.find("(") > 7)
    return false;
  size_t len = arg.copy(typeArg,arg.find("("),0);
  typeArg[len] = '\0';
  for(std::map<std::string, int>::iterator p = _myFunc.begin(); p != _myFunc.end(); ++p)
    {
      if (typeArg == p->first && p->second == 2)
	{
	  this->_typeArg = typeArg;
	  if (checkNbr(arg) == true)
	    return true;
	}
    }
  return false;
}

bool			checkLine::checkArg()
{
  std::string		cmd;
  std::string		arg;
  std::string		error;
  std::istringstream	iss (_line);


 
  iss >> cmd >> std::ws;
  iss >> arg >> std::ws;
  iss >> error >> std:: ws;

  if (error != "")
    return false;
  for(std::map<std::string, int>::iterator p = _myFunc.begin(); p != _myFunc.end(); ++p)
    {
      if (cmd == p->first && p->second == 1)
	{
	  this->_cmd = cmd;
	  if (checkValue(arg) == true)
	    return true;
	}
    }
  return false;
}

bool checkLine::validFunc()
{
  if (_line[0] == ';' || _line == "")
    return true;
  for(std::map<std::string, int>::iterator p = _myFunc.begin(); p != _myFunc.end(); ++p)
    {
      if (_line == p->first && p->second == 0)
	{
	  this->_cmd = _line;
	  this->_instr = _cmd;
	  return true;
	}
    }
  if (checkArg() == true)
    return true;
  else
    throw myException("function synthax\n");
}


std::string checkLine::getCmd()
{
  return _cmd;
}

std::string checkLine::getTypeArg()
{
  return _typeArg;
}

std::string checkLine::getValue()
{
  return _value;
}

std::string checkLine::getInstr()
{
  return _instr;
}
