//
// OperandTemplate.hpp for OperandTemplate in /home/bengle_b/rendu/cpp_abstractvm/files
// 
// Made by Bengler Bastien
// Login   <bengle_b@epitech.net>
// 
// Started on  Tue Feb 17 11:23:07 2015 Bengler Bastien
// Last update Mon Feb 23 10:19:24 2015 Bengler Bastien
//

#ifndef OPERANDTEMPLATE_HPP_
# define OPERANDTEMPLATE_HPP_

#include <iostream>
#include "IOperand.hpp"

template<typename T>
class	OperandTemplate : public IOperand
{
public:

  OperandTemplate(const std::string & value, eOperandType precision);
  virtual ~OperandTemplate();

  virtual std::string const & toString() const;
  virtual int getPrecision() const;
  virtual eOperandType getType() const;

  virtual IOperand * operator+(const IOperand &rhs) const;
  virtual IOperand * operator-(const IOperand &rhs) const;
  virtual IOperand * operator*(const IOperand &rhs) const;
  virtual IOperand * operator/(const IOperand &rhs) const;
  virtual IOperand * operator%(const IOperand &rhs) const;
private:
  std::string _string_value;
  eOperandType _precision;
};

#endif /* !OPERANDTEMPLATE_HPP_ */
