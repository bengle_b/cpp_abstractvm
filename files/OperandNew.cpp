//
// OperandNew.cpp for OperandNew in /home/bengle_b/rendu/cpp_abstractvm/files
// 
// Made by Bengler Bastien
// Login   <bengle_b@epitech.net>
// 
// Started on  Tue Feb 17 16:25:14 2015 Bengler Bastien
// Last update Mon Feb 23 15:43:08 2015 Bengler Bastien
//

#include <stdlib.h>
#include <iostream>
#include "OperandNew.hpp"
#include "OperandTemplate.hpp"
#include "my_OperationT.hpp"
#include "IOperand.hpp"

OperandNew::OperandNew()
{
  fptr_create[0] = &OperandNew::createInt8;
  fptr_create[1] = &OperandNew::createInt16;
  fptr_create[2] = &OperandNew::createInt32;
  fptr_create[3] = &OperandNew::createFloat;
  fptr_create[4] = &OperandNew::createDouble;
  fptr_whoisBigger[0] = &OperandNew::CharisBigger;
  fptr_whoisBigger[1] = &OperandNew::ShortIntisBigger;
  fptr_whoisBigger[2] = &OperandNew::IntisBigger;
  fptr_whoisBigger[3] = &OperandNew::FloatisBigger;
  fptr_whoisBigger[4] = &OperandNew::DoubleisBigger;
}

OperandNew::~OperandNew()
{
}

IOperand	*OperandNew::createOperand(eOperandType type, const std::string & value)
{
  return ((this->*fptr_create[type])(value));
}

IOperand	*OperandNew::ProcessOperation(eOperandType type,
					const IOperand *first,
					const IOperand *second, 
					const eOperation index)
{
  return ((this->*fptr_whoisBigger[type])(first, second, index));
}

IOperand	*OperandNew::createInt8(const std::string & value)
{
  IOperand * op = new OperandTemplate<char>(value, Int8);
  return op;
}

IOperand	*OperandNew::createInt16(const std::string & value)
{
  IOperand * op = new OperandTemplate<short int>(value, Int16);
  return op;
}

IOperand	*OperandNew::createInt32(const std::string & value)
{
  IOperand * op = new OperandTemplate<int>(value, Int32);
  return op;
}

IOperand	*OperandNew::createFloat(const std::string & value)
{
  IOperand * op = new OperandTemplate<float>(value, Float);
  return op;
}

IOperand	*OperandNew::createDouble(const std::string & value)
{
  IOperand * op = new OperandTemplate<double>(value, Double);
  return op;
}

IOperand	*OperandNew::CharisBigger(const IOperand *first,
					  const IOperand* second,
					  const eOperation index)
{
  IOperand *(*tab_ptr[5])(const IOperand *first, const IOperand *second) =
    {&make_add<char>, &make_sous<char>, &make_mul<char>,
     &make_div<char>, &make_mod<char>};
  return tab_ptr[index](first, second);
}

IOperand	*OperandNew::ShortIntisBigger(const IOperand *first,
					      const IOperand* second,
					      const eOperation index)
{
  IOperand *(*tab_ptr[5])(const IOperand *first, const IOperand *second) =
    {&make_add<short int>, &make_sous<short int>, &make_mul<short int>,
     &make_div<short int>, &make_mod<short int>};
  return tab_ptr[index](first, second);
}

IOperand	*OperandNew::IntisBigger(const IOperand *first,
					 const IOperand* second,
					 const eOperation index)
{
  IOperand *(*tab_ptr[5])(const IOperand *first, const IOperand *second) =
    {&make_add<int>, &make_sous<int>, &make_mul<int>,
     &make_div<int>, &make_mod<int>};
  return tab_ptr[index](first, second);
}

IOperand	*OperandNew::FloatisBigger(const IOperand *first,
					   const IOperand* second,
					   const eOperation index)
{
  IOperand *(*tab_ptr[5])(const IOperand *first, const IOperand *second) =
    {&make_add<float>, &make_sous<float>, &make_mul<float>,
     &make_div<float>, &make_mod<float>};
  return tab_ptr[index](first, second);
}

IOperand	*OperandNew::DoubleisBigger(const IOperand *first,
					    const IOperand* second,
					    const eOperation index)
{
  IOperand *(*tab_ptr[5])(const IOperand *first, const IOperand *second) =
    {&make_add<double>, &make_sous<double>, &make_mul<double>,
     &make_div<double>, &make_mod<double>};
  return tab_ptr[index](first, second);
}
