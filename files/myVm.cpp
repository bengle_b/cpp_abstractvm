//
// myVm.cpp for vm in /home/charti_t/test/cpp_abstractvm/files
// 
// Made by Thomas Chartier
// Login   <charti_t@epitech.net>
// 
// Started on  Sat Feb 21 16:07:30 2015 Thomas Chartier
// Last update Thu Feb 26 14:32:22 2015 Thomas Chartier
//

#include <limits>
#include "myVm.hpp"
#include "OperandNew.hpp"
#include "OperandTemplate.hpp"
#include "my_OperationT.hpp"
#include "exception.hpp"


myVm::myVm()
{
  this->ptrFunc["pop"] = &myVm::pop;
  this->ptrFunc["dump"] = &myVm::dump;
  this->ptrFunc["add"] = &myVm::add;
  this->ptrFunc["sub"] = &myVm::sub;
  this->ptrFunc["mul"] = &myVm::mul;
  this->ptrFunc["div"] = &myVm::div;
  this->ptrFunc["mod"] = &myVm::mod;
  this->ptrFunc["print"] = &myVm::print;
  this->createOp["int8"] = &myVm::createInt8;
  this->createOp["int16"] = &myVm::createInt16;
  this->createOp["int32"] = &myVm::createInt32;
  this->createOp["float"] = &myVm::createFloat;
  this->createOp["double"] = &myVm::createDouble;
}

myVm::~myVm()
{
}

void	myVm::exeCmd(std::string const &cmd)
{
  (this->*ptrFunc[cmd])();
}

void  myVm::exeCmdArg(std::string const &cmd,std::string const &arg,std::string const &value)
{
  if (cmd == "push")
    this->push(arg,value);
  else
    this->assert(arg,value);
}

void	myVm::push(std::string const &arg, std::string const &value)
{
  this->vmstack.push((this->*createOp[arg])(value));
}

void	myVm::pop()
{
  if (!vmstack.empty())
    vmstack.pop();
}

void	myVm::dump()
{
  if (!vmstack.empty())
    {
      std::queue<IOperand *> tmp = vmstack;
      
      while (!tmp.empty())
	{
	  std::cout << tmp.front()->toString() << std::endl;
	  tmp.pop();
	}
    }
}

void	myVm::assert(std::string const &arg, std::string const &value)
{
  IOperand *nb;
  IOperand *nb2;

  if (!vmstack.empty())
    {
      nb = (this->*createOp[arg])(value);
      nb2 = vmstack.front();
      if (!((nb->getType() == nb2->getType()) &&
	  (nb->toString() == nb2->toString())))
	throw myException("assert value doesn't match");
    }
}

void	myVm::add()
{
  IOperand *op1;
  IOperand *op2;
  IOperand *res;

  if (vmstack.size() >= 2)
    {
      op1 = vmstack.front();
      vmstack.pop();
      op2 = vmstack.front();
      vmstack.pop();
      res = *op1 + *op2;
      this->vmstack.push(res);
    }
  else
    throw myException("need at least 2 value in the stack");
}

void	myVm::sub()
{
  IOperand *op1;
  IOperand *op2;
  IOperand *res;

  if (vmstack.size() >= 2)
    {
      op1 = vmstack.front();
      vmstack.pop();
      op2 = vmstack.front();
      vmstack.pop();
      res = *op1 - *op2;
      this->vmstack.push(res);
    }
  else
    throw myException("need at least 2 value in the stack");
}

void	myVm::mul()
{
  IOperand *op1;
  IOperand *op2;
  IOperand *res;

  if (vmstack.size() >= 2)
    {
      op1 = vmstack.front();
      vmstack.pop();
      op2 = vmstack.front();
      vmstack.pop();
      res = *op1 * *op2;
      this->vmstack.push(res);
    }
  else
    throw myException("need at least 2 value in the stack");
}

void	myVm::div()
{
  IOperand *op1;
  IOperand *op2;
  IOperand *res;

  if (vmstack.size() >= 2)
    {
      op1 = vmstack.front();
      vmstack.pop();
      op2 = vmstack.front();
      if (op2->toString() == "0")
	throw myException("division by 0");
      vmstack.pop();
      res = *op1 / *op2;
      this->vmstack.push(res);
    }
  else
    throw myException("need at least 2 value in the stack");
}

void	myVm::mod()
{
  IOperand *op1;
  IOperand *op2;
  IOperand *res;

  if (vmstack.size() >= 2)
    {
      op1 = vmstack.front();
      vmstack.pop();
      op2 = vmstack.front();
      if (op2->toString() == "0")
        throw myException("modulo by 0");
      vmstack.pop();
      res = *op1 % *op2;
      this->vmstack.push(res);
    }
  else
    throw myException("need at least 2 value in the stack");
}

void	myVm::print()
{
  std::stringstream ss;
  int	tmp;

  if (this->vmstack.front()->getType() == Int8)
    {
      ss << vmstack.front()->toString().c_str();
      ss >> tmp;
      std::cout << (char)tmp << std::endl;
    }
  else
    throw myException("can't print not an int8");
}

IOperand *myVm::createInt8(std::string const &str)
{
  OperandNew	tmp;
  IOperand	*nb;
  std::stringstream	ss;
  int		op;

  nb = tmp.createOperand(Int8, str);
  ss << nb->toString();
  ss >> op;
  if(op < std::numeric_limits<char>::min())
     throw myException("char underflow");
  if(op > std::numeric_limits<char>::max())
    throw myException("char overflow");

  return(nb);
}

IOperand *myVm::createInt16(std::string const &str)
{
  OperandNew    tmp;
  IOperand      *nb;
  std::stringstream     ss;
  int           op;

  nb = tmp.createOperand(Int16, str);
  ss << nb->toString();
  ss >> op;
  if(op < std::numeric_limits<short int>::min())
    throw myException("short int underflow");
  if(op > std::numeric_limits<short int>::max())
    throw myException("short int overflow");

  return(nb);
}

IOperand *myVm::createInt32(std::string const &str)
{
  OperandNew    tmp;
  IOperand      *nb;
  std::stringstream     ss;
  double           op;

  nb = tmp.createOperand(Int32, str);
  ss << nb->toString();
  ss >> op;
  if(op < std::numeric_limits<int>::min())
    throw myException("int underflow");
  if(op > std::numeric_limits<int>::max())
    throw myException("int overflow");

  return(nb);
}

IOperand *myVm::createFloat(std::string const &str)
{
  OperandNew    tmp;
  IOperand      *nb;
  std::stringstream     ss;
  double           op;

  nb = tmp.createOperand(Float, str);
  ss << nb->toString();
  ss >> op;
  if(op < std::numeric_limits<float>::min())
    throw myException("float underflow");
  if(op > std::numeric_limits<float>::max())
    throw myException("float overflow");

  return(nb);
}

IOperand *myVm::createDouble(std::string const &str)
{
  OperandNew    tmp;
  IOperand      *nb;
  std::stringstream     ss;
  double           op;

  nb = tmp.createOperand(Double, str);
  ss << nb->toString();
  ss >> op;
  if(op < -std::numeric_limits<double>::infinity())
    throw myException("double underflow");
  if(op > std::numeric_limits<double>::infinity())
    throw myException("double overflow");

  return(nb);

}
