//
// my_OperationT.hpp for my_OperationT in /home/bengle_b/rendu/cpp_abstractvm
// 
// Made by Bengler Bastien
// Login   <bengle_b@epitech.net>
// 
// Started on  Sun Feb 22 16:16:43 2015 Bengler Bastien
// Last update Thu Feb 26 14:24:56 2015 Thomas Chartier
//

#ifndef MY_OPERATIONT_HPP
# define MY_OPERATIONT_HPP

#include "IOperand.hpp"

template<typename X>
IOperand*	make_add(const IOperand* first, const IOperand* second);

template<typename X>
IOperand*	make_sous(const IOperand* first, const IOperand* second);

template<typename X>
IOperand*	make_mul(const IOperand* first, const IOperand* second);

template<typename X>
IOperand*	make_div(const IOperand* first, const IOperand* second);

template<typename X>
IOperand*	make_mod(const IOperand* first, const IOperand* second);

std::string	add_two_char(const IOperand* first,
			 const IOperand* second);

std::string	sous_two_char(const IOperand* first,
			 const IOperand* second);

std::string	mul_two_char(const IOperand* first,
			 const IOperand* second);

std::string	div_two_char(const IOperand* first,
			 const IOperand* second);

template<typename X>
X my_mod(X x, X y);

long	my_abs(long value);

/*std::string	mod_two_char(const IOperand* first,
  const IOperand* second);*/

#endif /* !MY_OPERATIONT_HPP */
