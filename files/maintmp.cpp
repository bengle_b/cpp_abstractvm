//
// main.hpp for main in /home/bengle_b/rendu/cpp_abstractvm/files
// 
// Made by Bengler Bastien
// Login   <bengle_b@epitech.net>
// 
// Started on  Mon Feb 16 15:26:24 2015 Bengler Bastien
// Last update Sat Feb 21 14:10:47 2015 Thomas Chartier
//

#include <iostream>
#include <fstream>
#include "exception.hpp"
#include "checkLine.hpp"
#include "IOperand.hpp"
#include "OperandInt8.hpp"
#include "OperandInt16.hpp"
#include "OperandInt32.hpp"
#include "OperandFloat.hpp"
#include "OperandDouble.hpp"
#include "myStack.hpp"

int	main(int ac, char **av)
{
  std::string line;
  checkLine   *cLine;
  myStack     myStack;

  if(ac > 1)
    {
      std::ifstream file(av[1], std::ios::in);
      if(!file)
	throw myException("file not found");
      do
	{
	  if(!getline(file, line))
	    throw myException("No end or ;;\n");
	  cLine = new checkLine(line.c_str());
	  myStack.push(cLine->getInstr());
	  delete cLine;
	}       while(line != ";;" && line != "exit");
      myStack.doTask();
    }
  else
    {
      do
	{
	  getline(std::cin, line, '\n');
	  cLine = new checkLine(line.c_str());
	  delete cLine;
	}       while(line != ";;" && line != "exit");
    }
}
