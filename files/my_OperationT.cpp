//
// my_OperationT.cpp for my_OperationT in /home/bengle_b/rendu/cpp_abstractvm
// 
// Made by Bengler Bastien
// Login   <bengle_b@epitech.net>
// 
// Started on  Sun Feb 22 16:18:56 2015 Bengler Bastien
// Last update Thu Feb 26 14:33:35 2015 Thomas Chartier
//

#include <iostream>
#include <sstream>
#include <string>
#include <limits>
#include <math.h>
#include "IOperand.hpp"
#include "my_OperationT.hpp"
#include "OperandTemplate.hpp"
#include "exception.hpp"

template<typename X>
IOperand*	make_add(const IOperand* first, const IOperand* second)
{
  IOperand	*result;
  std::stringstream	ss;
  X	tmp, tmp2;

  if (first->getPrecision() == 0)
    ss << add_two_char(first, second);
  else
    {
      ss << first->toString().c_str();
      ss >> tmp;
      ss.clear();
      ss.str(second->toString().c_str());
      ss >> tmp2;
      if ((first->getPrecision() >= 3 &&
	   tmp + tmp2 == std::numeric_limits<X>::infinity()) ||
	  (first->getPrecision() >= 3 &&
	   tmp + tmp2 == -std::numeric_limits<X>::infinity()))
	throw myException("Overflow");
      else if ((first->getPrecision() < 3) &&
	       (tmp + tmp2 < std::numeric_limits<X>::min() ||
		tmp + tmp2 > std::numeric_limits<X>::max()))
	throw myException("Overflow");
      tmp = tmp + tmp2;
      ss.str(std::string());
      ss.clear();
      ss << tmp;
    }
  return result = new OperandTemplate<X>(ss.str(), first->getType());
}

std::string	add_two_char(const IOperand* first, const IOperand* second)
{
  char	tmp;
  int	save;
  std::stringstream ss;
  std::string res;

  ss << first->toString().c_str();  
  ss >> save;
  tmp = save;
  ss.clear();
  ss.str(second->toString().c_str());
  ss >> save;
  if (tmp + (char)save > std::numeric_limits<char>::max() ||
      tmp + (char)save < std::numeric_limits<char>::min())
    throw myException("Overflow");
  tmp = tmp + save;
  ss.clear();
  ss << (int)tmp;
  res = ss.str();
  return res;
}

template<typename X>
IOperand*	make_sous(const IOperand* first, const IOperand* second)
{
  IOperand *result;
  std::stringstream ss;
  X tmp, tmp2;
  int save;

  if (first->getPrecision() == 0 && second->getPrecision() == 0)
    ss << sous_two_char(first, second);
  else
    {
      ss << first->toString().c_str();
      ss >> tmp;
      ss.clear();
      ss.str(second->toString().c_str());
      ss >> tmp2;
      if (((first->getPrecision() >= 3 || second->getPrecision() >= 3) &&
	   tmp - tmp2 == std::numeric_limits<X>::infinity()) ||
	  ((first->getPrecision() >= 3 || second->getPrecision() >= 3) &&
	   tmp - tmp2 == -std::numeric_limits<X>::infinity()))
	throw myException("Overflow");
      else if (((first->getPrecision() < 3 || second->getPrecision() < 3) &&
		(tmp - tmp2 < std::numeric_limits<X>::min() ||
		 tmp - tmp2 > std::numeric_limits<X>::max())))
	throw myException("Overflow");
      tmp = tmp - tmp2;
      ss.clear();
      ss << tmp;
    }
  if (first->getPrecision() > second->getPrecision())
    return result = new OperandTemplate<X>(ss.str(), first->getType());
  return result = new OperandTemplate<X>(ss.str(), second->getType());
}

std::string	sous_two_char(const IOperand* first, const IOperand* second)
{
  char	tmp;
  int	save;
  std::stringstream ss;
  std::string res;

  ss << first->toString().c_str();
  ss >> save;
  tmp = save;
  ss.clear();
  ss.str(second->toString().c_str());
  ss >> save;
  if (tmp - (char)save > std::numeric_limits<char>::max() ||
      tmp - (char)save < std::numeric_limits<char>::min())
    throw myException("Overflow");
  tmp = tmp - save;
  ss.str(std::string());
  ss.clear();
  ss << (int)tmp;
  res = ss.str();
  return res;
}

template<typename X>
IOperand*	make_mul(const IOperand* first, const IOperand* second)
{
  IOperand *result;
  std::stringstream ss;
  X tmp, tmp2;
  int save;
  if (first->getPrecision() == 0 && second->getPrecision() == 0)
    ss << mul_two_char(first, second);
  else
    {
      ss << first->toString().c_str();
      ss >> tmp;
      ss.clear();
      ss.str(second->toString().c_str());
      ss >> tmp2;
      if (((first->getPrecision() >= 3 || second->getPrecision() >= 3) &&
	   tmp * tmp2 == std::numeric_limits<X>::infinity()) ||
	  ((first->getPrecision() >= 3 || second->getPrecision() >= 3) &&
	   tmp * tmp2 == -std::numeric_limits<X>::infinity()))
	throw myException("Overflow");
      else if (((first->getPrecision() < 3 || second->getPrecision() < 3) &&
		(tmp * tmp2 < std::numeric_limits<X>::min() ||
		 tmp * tmp2 > std::numeric_limits<X>::max())))
	throw myException("Overflow");
      tmp = tmp * tmp2;
      ss.clear();
      ss << tmp;
    }
  if (first->getPrecision() > second->getPrecision())
    return result = new OperandTemplate<X>(ss.str(), first->getType());
  return result = new OperandTemplate<X>(ss.str(), second->getType());
}

std::string	mul_two_char(const IOperand* first,
			 const IOperand* second)
{
  char	tmp;
  int	save;
  std::stringstream ss;
  std::string res;

  ss << first->toString().c_str();  
  ss >> save;
  tmp = save;
  ss.clear();
  ss.str(second->toString().c_str());
  ss >> save;
  if (tmp * (char)save > std::numeric_limits<char>::max() ||
      tmp * (char)save < std::numeric_limits<char>::min())
    throw myException("Overflow");
  tmp = tmp * save;
  ss.str(std::string());
  ss.clear();
  ss << (int)tmp;
  res = ss.str();
  return res;
}

template<typename X>
IOperand*	make_div(const IOperand* first, const IOperand* second)
{
  IOperand *result;
  std::stringstream ss;
  X tmp, tmp2;
  int save;

  if (first->getPrecision() == 0)
    ss << div_two_char(first, second);
  else
    {
      ss << first->toString().c_str();
      ss >> tmp;
      ss.clear();
      ss.str(second->toString().c_str());
      ss >> tmp2;
      if (tmp2 == 0)
	throw myException("Div by 0");
      if (((first->getPrecision() >= 3 || second->getPrecision() >= 3) &&
	   tmp / tmp2 == std::numeric_limits<X>::infinity()) ||
	  ((first->getPrecision() >= 3 || second->getPrecision() >= 3) &&
	   tmp / tmp2 == -std::numeric_limits<X>::infinity()))
	throw myException("Overflow");
      else if (((first->getPrecision() < 3 || second->getPrecision() < 3) &&
		(tmp / tmp2 < std::numeric_limits<X>::min() ||
		 tmp / tmp2 > std::numeric_limits<X>::max())))
	throw myException("Overflow");
      tmp = tmp / tmp2;
      ss.clear();
      ss << tmp;
    }
  if (first->getPrecision() > second->getPrecision())
    return result = new OperandTemplate<X>(ss.str(), first->getType());
  return result = new OperandTemplate<X>(ss.str(), second->getType());
}

std::string	div_two_char(const IOperand* first,
			 const IOperand* second)
{
  char	tmp;
  int	save;
  std::stringstream ss;
  std::string res;

  ss << first->toString().c_str();  
  ss >> save;
  tmp = save;
  ss.clear();
  ss.str(second->toString().c_str());
  ss >> save;
  if (save == 0)
    throw myException("Div by 0");
  if (tmp / (char)save > std::numeric_limits<char>::max() ||
      tmp / (char)save < std::numeric_limits<char>::min())
    throw myException("Overflow");
  tmp = tmp / save;
  ss.str(std::string());
  ss.clear();
  ss << (int)tmp;
  res = ss.str();
  return res;
}

long my_abs(long value)
{
  if (value < 0)
    return -value;
  else
    return value;
}

template<typename X>
X my_mod(X x, X y)
{
  x -= y*my_abs(x/y);
  if (x >  0.)
    return x;
  else
    {
      x = 0;
      return x;
    }
}

template<typename X>
IOperand*	make_mod(const IOperand* first, const IOperand* second)
{
  IOperand	*result;
  std::stringstream ss;
  X	tmp, tmp2;
  int	save;

  if (first->getPrecision() == 0)
    ss << div_two_char(first, second);
  else
    {
      ss << first->toString().c_str();
      ss >> tmp;
      ss.clear();
      ss.str(second->toString().c_str());
      ss >> tmp2;
      if (tmp2 == 0)
	throw myException("Div by 0");
      if (((first->getPrecision() >= 3 || second->getPrecision() >= 3) &&
	   my_mod(tmp, tmp2) == std::numeric_limits<X>::infinity()) ||
	  ((first->getPrecision() >= 3 || second->getPrecision() >= 3) &&
	   my_mod(tmp, tmp2) == -std::numeric_limits<X>::infinity()))
	throw myException("Overflow");
      else if (((first->getPrecision() < 3 || second->getPrecision() < 3) &&
		(my_mod(tmp, tmp2) < std::numeric_limits<X>::min() ||
		 my_mod(tmp, tmp2) > std::numeric_limits<X>::max())))
	throw myException("Overflow");
      tmp = my_mod(tmp, tmp2);
      ss.clear();
      ss << tmp;
    }
  if (first->getPrecision() > second->getPrecision())
    return result = new OperandTemplate<X>(ss.str(), first->getType());
  return result = new OperandTemplate<X>(ss.str(), second->getType());
}

std::string	mod_two_char(const IOperand* first,
			     const IOperand* second)
{
  char	tmp;
  int	save;
  std::stringstream ss;
  std::string res;

  ss << first->toString().c_str();  
  ss >> save;
  tmp = save;
  ss.clear();
  ss.str(second->toString().c_str());
  ss >> save;
  if (save == 0)
    throw myException("Div by 0");
  if (tmp % (char)save > std::numeric_limits<char>::max() ||
      tmp % (char)save < std::numeric_limits<char>::min())
    throw myException("Overflow");
  tmp = tmp / save;
  ss.str(std::string());
  ss.clear();
  ss << (int)tmp;
  res = ss.str();
  return res;
}

template IOperand*	make_add<char>(const IOperand* first, const IOperand* second);
template IOperand*	make_add<short int>(const IOperand* first, const IOperand* second);
template IOperand*	make_add<int>(const IOperand* first, const IOperand* second);
template IOperand*	make_add<float>(const IOperand* first, const IOperand* second);
template IOperand*	make_add<double>(const IOperand* first, const IOperand* second);

template IOperand*	make_sous<char>(const IOperand* first, const IOperand* second);
template IOperand*	make_sous<short int>(const IOperand* first, const IOperand* second);
template IOperand*	make_sous<int>(const IOperand* first, const IOperand* second);
template IOperand*	make_sous<float>(const IOperand* first, const IOperand* second);
template IOperand*	make_sous<double>(const IOperand* first, const IOperand* second);

template IOperand*	make_mul<char>(const IOperand* first, const IOperand* second);
template IOperand*	make_mul<short int>(const IOperand* first, const IOperand* second);
template IOperand*	make_mul<int>(const IOperand* first, const IOperand* second);
template IOperand*	make_mul<float>(const IOperand* first, const IOperand* second);
template IOperand*	make_mul<double>(const IOperand* first, const IOperand* second);

template IOperand*	make_div<char>(const IOperand* first, const IOperand* second);
template IOperand*	make_div<short int>(const IOperand* first, const IOperand* second);
template IOperand*	make_div<int>(const IOperand* first, const IOperand* second);
template IOperand*	make_div<float>(const IOperand* first, const IOperand* second);
template IOperand*	make_div<double>(const IOperand* first, const IOperand* second);

template IOperand*	make_mod<char>(const IOperand* first, const IOperand* second);
template IOperand*	make_mod<short int>(const IOperand* first, const IOperand* second);
template IOperand*	make_mod<int>(const IOperand* first, const IOperand* second);
template IOperand*	make_mod<float>(const IOperand* first, const IOperand* second);
template IOperand*	make_mod<double>(const IOperand* first, const IOperand* second);

template char		my_mod<char>(char x, char y);
template short int	my_mod<short int>(short int x, short int y);
template int		my_mod<int>(int x, int y);
template float		my_mod<float>(float x, float y);
template double		my_mod<double>(double x, double y);
