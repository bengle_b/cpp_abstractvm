//
// Operand_misc.hpp for Operand_misc in /home/bengle_b/rendu/cpp_abstractvm/files
// 
// Made by Bengler Bastien
// Login   <bengle_b@epitech.net>
// 
// Started on  Mon Feb 16 16:03:06 2015 Bengler Bastien
// Last update Sun Feb 22 16:22:10 2015 Bengler Bastien
//

#ifndef OPERANDNEW_HPP_
# define OPERANDNEW_HPP_

#include <iostream>
#include <sstream>
#include <string>
#include "IOperand.hpp"
#include "OperandTemplate.hpp"

class	OperandNew
{
public:

  OperandNew();
  virtual ~OperandNew();

  IOperand * createOperand(eOperandType type, const std::string & value);
  IOperand * ProcessOperation(eOperandType type, const IOperand *first,
			const IOperand *second, const eOperation index);

private:
  IOperand * (OperandNew::*fptr_create[5])(const std::string & value);
  IOperand * (OperandNew::*fptr_whoisBigger[5])(const IOperand *first,
						const IOperand* second,
						const eOperation index);
  IOperand * createInt8(const std::string & value);
  IOperand * createInt16(const std::string & value);
  IOperand * createInt32(const std::string & value);
  IOperand * createFloat(const std::string & value);
  IOperand * createDouble(const std::string & value);

  IOperand * CharisBigger(const IOperand *first, const IOperand* second,
			  const eOperation index) ;
  IOperand * ShortIntisBigger(const IOperand *first, const IOperand* second,
			      const eOperation index);
  IOperand * IntisBigger(const IOperand *first, const IOperand* second,
			 const eOperation index);
  IOperand * FloatisBigger(const IOperand *first, const IOperand* second,
			   const eOperation index);
  IOperand * DoubleisBigger(const IOperand *first, const IOperand* second,
			    const eOperation index);
};

#endif /* !OPERANDNEW_HPP_ */
