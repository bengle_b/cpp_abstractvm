##
## Makefile for Makefile in /home/bengle_b/rendu/psu_2014_malloc
## 
## Made by Bengler Bastien
## Login   <bengle_b@epitech.net>
## 
## Started on  Fri Jan 30 11:25:24 2015 Bengler Bastien
## Last update Sun Feb 22 16:52:23 2015 Thomas Chartier
##

NAME		= abstract

SRC		= files/main.cpp \
		  files/exception.cpp \
		  files/checkLine.cpp \
		  files/myStack.cpp \
		  files/myVm.cpp \
		  files/OperandNew.cpp \
		  files/OperandTemplate.cpp \
		  files/my_OperationT.cpp


OBJ		= $(SRC:.cpp=.o)

CC		= g++

RM		= rm -f

CFLAGS		= -Wall -Wextra -Werror


all: $(NAME)

$(NAME): $(OBJ)
	$(CC) $(OBJ) -o $(NAME)

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: clean fclean re all
